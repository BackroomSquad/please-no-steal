public class Level {
  //Initializes variables
  final int lvWidth = 20, lvHeight = 15;
  final float gravity = -0.7;
  //Sets the player position to 300, 300
  Player player = new Player(300, 300);
  //Makes an array of blocks with the size of width*height of level
  Block[] blocks = new Block[lvWidth*lvHeight];
  //Initializes LevelLoader
  LevelLoader ll = new LevelLoader();
  private int currentLevelID;
  public boolean levelLoading;
  public boolean levelReset;
  //Initializes list of Entities
  public ArrayList<Entity> entities = new ArrayList<Entity>();
  //Win variable, returns true if there is no more levels to be loaded
  public boolean win;
  private TextLoader tl;
  //Reset animation
  public int resetAnimation;
  public float playerX, playerY;
  //Timer
  private float timer;
  private float lastTime = -1;

  public Level() {
    //Adds player to list of entities
    addEntity(player);
    
    //Goes through all blocks and initialzes them
    for (int i = 0; i < blocks.length; i++) {
      int x = i%lvWidth;
      int y = i/lvWidth;
      blocks[i] = new Block(x, y);
    }
    
    tl = new TextLoader("LevelText.txt");
  }
  
  //Updates level
  public void update() {
    if (lastTime == -1) lastTime = System.nanoTime()/1000000000.0;
    //If player has won, then don't update anything
    if (win) {
      //If the player hits the reset button, then set the level ID to 0 and load next level
      if (levelReset) {
        timer = 0;
        lastTime = System.nanoTime()/1000000000.0;
        currentLevelID = 0;
        loadNextLevel();
        levelReset = false;
      }
      
      return;
    }
    
    //If levelReset is true, then add one to resetAnimation, and if that gets over 30, then animation is over and level should run normal code 
    if (levelReset) {
      resetAnimation++;
      
      if (resetAnimation <= 40) return;
      resetAnimation = 0;
    }

    timer += (System.nanoTime()/1000000000.0)-lastTime;
    lastTime = (System.nanoTime()/1000000000.0);
    
    //If levelLoad is pending, then load level, this is done to avoid errors form loading new level
    if (levelLoading) {
      loadNextLevel();
      levelLoading = false;
    }
    
    //If levelResset is pending, then reset level
    if (levelReset) {
      reset();
      levelReset = false;
    }
    
    //Updates all entites
    for (int i = 0; i < entities.size(); i++) {
      Entity e = entities.get(i);
      e.update();
      
      if (e instanceof Particle && ((Particle) e).destroy) entities.remove(i--);
    }
    
    //Updates all blocks
    for (Block b : blocks) {
      b.update();
    }
  }
  
  //Returns specific block based on x and y coordinates
  public Block getBlock(int x, int y) {
    //If the coordinate is out of bounds, return null
    if (x < 0 || y < 0 || x >= lvWidth || y >= lvHeight) return null;
    
    return blocks[x+y*lvWidth];
  }
  
  public void addEntity(Entity e) {
    entities.add(e);
  }
  
  //Returns list of entities within a AABB (Axis-Alligned Bounding Box)
  public ArrayList<Entity> getEntityWithinAABB(float x, float y, float w, float h) {
    ArrayList<Entity> result = new ArrayList<Entity>();
    
    for (Entity e : entities) {
      if (e.x > x && e.x < w && e.y > y && e.y < h) result.add(e);
    }
    
    return result;
  }
  
  public void render() {
    //Renders all entites
    for (Entity e : entities) {
      e.render();
    }
    
    //Goes through all blocks and calls their render
    for (int i = 0; i < blocks.length; i++) {
      blocks[i].render();
    }
    
    //Renderes flavor text at the top of the level
    fill(0);
    textSize(20);
    String flavorText = tl.getString(currentLevelID);
    text(flavorText, width/2-textWidth(flavorText)/2, 32-5);
    
    int minute = (int)timer/60;
    float seconds = timer%60;
    text(minute+":"+(seconds<10?"0":"")+round(seconds*100)/100.0, width-textWidth("0:00.00")-5, height-10);
    
    if (win) {
      //Here goes code to render win screen
      
      //Adds shadow by offsetting text
      fill(0);
      textSize(65);
      //Left
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2+1);
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2-0);
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2-1);
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2-2);
      
      //Second column
      text("You're winner", width/2-textWidth("You're winner")/2-1, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2-1, height/2-2);
      
      //Third column
      text("You're winner", width/2-textWidth("You're winner")/2-0, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2-0, height/2-2);
      
      //Fourth column 
      text("You're winner", width/2-textWidth("You're winner")/2+1, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2+1, height/2-2);
      
      //Right
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2+1);
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2-0);
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2-1);
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2-2);
      
      //Renders winning text with gold color and font size 65
      fill(255, 215, 0);
      textSize(65);
      //Centers the text by rendering the text from the screen width/2 minus the text width/2
      text("You're winner", width/2-textWidth("You're winner")/2, height/2);
    }
    
    if (resetAnimation > 30) {
      fill(255, 255, 255, 255/5*(resetAnimation-30));
      rect(0, 0, width, height);
    }
  }
  
  //Loads next level, only if there isn't a load currently going on
  public void loadNextLevel() {
    win = !ll.loadlevel(this, ++currentLevelID);
    levelLoading = false;
  }
  
  //Makes it easier to add a particle
  public void addParticle(float x, float y, int destroyTimer, float dirrection, float speed, Entity e) {
    entities.add(new Particle(x, y, destroyTimer, dirrection, speed, e));
  }
  
  //Makes it easier to add a particle
  public void addParticle(float x, float y, int destroyTimer, float dirrection, float speed, boolean useGravity, boolean destroyOnCollide, Entity e) {
    entities.add(new Particle(x, y, destroyTimer, dirrection, speed, useGravity, destroyOnCollide, e));
  }
  
  public void reset() {
    ll.loadlevel(this, currentLevelID);
  }
}
