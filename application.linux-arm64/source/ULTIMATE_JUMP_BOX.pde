Level level;
float titleScreenFrameCount;
boolean onTitle = true;
Title title = new Title();
static PApplet please_no_steal;

void setup() {
  //Sets the size of the window, the stroke colour and the strokeWeight
  size(640, 480);
  stroke(255);
  strokeWeight(0);
  surface.setTitle("SUPER ULTIMATE JUMP BOX! 64: TOURNAMENT EDITION 2000x CLIMAX REMIX & KNUCKLES");
  
  level = new Level();
  
  please_no_steal = this;
  
  //Loads level for testing
  level.loadNextLevel();
}

private int lastFrameCount;
private long lastTimeNano = System.nanoTime();
void draw() {
  //FPS
  if (System.nanoTime()-lastTimeNano >= 1000000000L) {
    println((frameCount-lastFrameCount)+"FPS");
    lastTimeNano = System.nanoTime();
    lastFrameCount = frameCount;
  }
  
  //If on title, then run different code
  if (onTitle) {
    title.render();
  } else {
    //Updating player and other code that needs it
    level.update();
    
    //Renderering screen 
    background(0);
    level.render();
  }
}

//Tests if different keys are pressed or not and sets variables accordingly.
void keyPressed() {
  if (keyCode == RIGHT || key == 'd') {
    level.player.xDirection = 1;
  }
  
  if (keyCode == LEFT || key == 'a') {
    level.player.xDirection = -1;
  }
  
  if (keyCode == UP || keyCode == DOWN) title.selected++;
  
  if (keyCode == DOWN) level.player.slideDown = true; 
  
  if (key == ' ') {
    if (onTitle) {
      title.select();
    } else {
      level.player.jump = true;
    }
  }
  
  if (key == 'r') level.levelReset = true;
}

void keyReleased() {
  if (keyCode == RIGHT || key == 'd') {
    level.player.xDirection = 0;
  }
  
  if (keyCode == LEFT || key == 'a') {
    level.player.xDirection = 0;
  }
  
  if (keyCode == DOWN) level.player.slideDown = false;
}
