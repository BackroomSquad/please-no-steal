public class Enemy extends Entity {
  public Enemy(float x, float y) {
    super(x, y);
  }
  
  //Checks if Entity collides with another Entity
  @Override
  public boolean collides(Entity e, float xOffs, float yOffs) {
    boolean collides = super.collides(e, xOffs, yOffs);
    
    //If enetity collides and it is with player, then reset level
    if (collides && e instanceof Player && !level.levelReset) {
      Sound.ENEMY_HIT.stop();
      Sound.ENEMY_HIT.play();
      level.levelReset = true;
    }
    
    return collides;
  }
}
