public class TextLoader {
  private String[] text;
  
  public TextLoader(String path) {
    text = loadStrings(path);
  }
  
  public String getString(int id) {
    if (id-1 >= text.length) return text[text.length-1];
    return text[id-1];
  }
}
