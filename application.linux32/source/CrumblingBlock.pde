public class CrumblingBlock extends Block {
  private int timer = 0;
  private boolean touched;
  private float vy;
  private float yFall;
  private int fallFrames;
  private ArrayList<int[]> crumbles = new ArrayList<int[]>();
 
  public CrumblingBlock(int x, int y, int fallFrames) {
    super(x, y);
    this.fallFrames = fallFrames;
    //Adds collition to the block
    solid = true;
  }
  
  public boolean collides(Entity e) {
     if (e instanceof Player) {
         touched = true;
    }
    
    return super.collides(e);
    
  }
  
  public void update() {
    if (touched) {
      timer++;
    }
    if (timer > fallFrames) {
      solid = false;
      vy += level.gravity;
      yFall += vy;
    }
    
    if ((float)timer/fallFrames > 0.1 && crumbles.size() == 0) {
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      Sound.CRUMBLING_BLOCK.play();
    }
    
    if ((float)timer/fallFrames > 0.4 && crumbles.size() == 1) {
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
    }
    
    if ((float)timer/fallFrames > 0.9 && crumbles.size() == 4) {
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      Sound.CRUMBLING_BLOCK.play();
    }
  }
  
  public void render() {
    fill(0);
    strokeWeight(3);
    stroke(255);
    
    rect(x*32, (int) height-y*32-32-yFall, 32, 32);
    noStroke();
    
    fill(255);
    for(int[] pos : crumbles) {
      rect(x*32+pos[0], height-y*32-32-yFall+pos[1], 5, 5);
    }
  }
}
