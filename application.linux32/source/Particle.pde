public class Particle extends Entity {
  public float direction;
  public float speed;
  public boolean effectedByGravity = true;
  public boolean destryOnCollide = true;
  public int destroyTimer;
  private int time;
  public boolean destroy;
  public Entity noCollision;
  
  //Simple constrcture
  public Particle(float x, float y, int destroyTimer, float direction, float speed, Entity noCollision) {
    super(x, y);
    
    this.direction = direction;
    this.speed = speed;
    this.noCollision = noCollision;
    this.destroyTimer = destroyTimer;
    
    vx = cos(radians(direction))*speed;
    vy = sin(radians(direction))*speed;
    
    size = 1;
  }
  
  //Specific constructer
  public Particle(float x, float y, int destroyTimer, float direction, float speed, boolean effectedByGravity, boolean destryOnCollide, Entity noCollision) {
    this(x, y, destroyTimer, direction, speed, noCollision);
    
    this.effectedByGravity = effectedByGravity;
    this.destryOnCollide = destryOnCollide;
  }
  
  @Override
  public void update() {
    //If the timer is greater than the destroyTimer, then destroy the particle
    if (time++ > destroyTimer)
      destroy();
    
    //If effectedByGravity is false, then add 0
    vy += effectedByGravity ? level.gravity : 0;
    
    //Moves particle
    move();
  }
  
  //Checks if Entity collides with another Entity
  @Override
  public boolean collides(float vx, float vy) {
    boolean col = super.collides(vx, vy);
    
    //If destryOnCollide is true, and particle collides, then destroy particle 
    if (destryOnCollide && col) {
      destroy();
    }
    
    return false;
  }
  
  //Checks if Entity collides with another Entity
  @Override
  public boolean collides(Entity e, float xOffs, float yOffs) {
    boolean col = super.collides(e, xOffs, yOffs);
    
    if (e instanceof Particle) return false;
    
    //If destryOnCollide is true, and particle collides, then destroy particle
    if (destryOnCollide && col) destroy();
    
    return col;
  }
  
  //Checks if Particle is out of bounds, then destroy
  @Override
  public void oob() {
    destroy();
  }
  
  public void destroy() {
    destroy = true;
  }
  
  public void render() {
    fill(255);
    stroke(255);
    strokeWeight(3);
    point(x, height-y-size);
    noStroke();
  }
}
