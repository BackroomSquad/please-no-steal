public class Player extends Entity {
  //Initializes variables speficly for the player
  public boolean jump;
  public float jumpSpeed = 11;
  public float wallDashSpeed = 10;
  public int xDirection;
  public float maxVel = 1;
  public boolean slideDown;
  private int nextParticle = -1;
  private boolean lastGrounded;
  private float lastVY;
  
  public Player(float x, float y) {
    super(x, y);
    
    speed = 3;
  }
  
  public void update() {
    if (nextParticle-- <= -1) nextParticle = (int) random(25)+5;
    
    //xDirecton is equal to the button held down.
    //vx is equal to the direction the player is moving in.
    //If xDirection*vx is positive the player is pressing the same direction as they are moving
    //That value has to be less than maxVel, because we only want the player to manipulate the velocity if it is below maxVel
    if (xDirection * vx < maxVel)
      vx += xDirection;
    
    if (nextParticle == 0 && grounded && onWall == 0 && xDirection * vx >= maxVel*0.9)
      level.addParticle(x-xDirection*(size/2+1)+size/2, y, 5, 90+xDirection*45, 10, this);
    
    //If player is on wall and player is moving in the same direction as the wall and player isn't trying to slide down and player doesn't move upwards, then reset vy, so player falls down slower
    if (onWall != 0 && onWall == -xDirection && !slideDown) {
      if (vy < 0) {
        vy = 0;
        if (nextParticle == 0) {
          if (onWall == 1) level.addParticle(x, y+size+5, 15, 80+random(20)-10, 5, this);
          else level.addParticle(x+size-5, y+size+5, 15, 90-80+90+random(20)-10, 5, this);
        }
      } else {
        if (nextParticle == 0) {
          if (onWall == 1) level.addParticle(x, y-5, 15, -80+random(20)-10, 5, this);
          else level.addParticle(x+size-5, y-5, 15, 90+80+90+random(20)-10, 5, this);
        }
      }
    }
    
    //Sets the players velocity based on gravity
    vy += level.gravity;
    
    //When the player is grounded and the jump button is pressed, the player accelerates up.
    //When the player is on a wall and not grounded and the jump button is pressed, the player jumps in the direction away from the wall and up.
    if (jump) {
      if (grounded) {
        vy = jumpSpeed;
        Sound.JUMP_SOUND.play();
      } else if (onWall != 0) {
        vy = jumpSpeed;
        vx = wallDashSpeed*onWall;
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90+10+random(5)-2.5, 8, false, false, this);
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90+5+random(5)-2.5, 8, false, false, this);
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90+random(5)-2.5, 8, false, false, this);
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90-5+random(5)-2.5, 8, false, false, this);
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90-10+random(5)-2.5, 8, false, false, this);
        Sound.WALL_JUMP.play();
      }
    }
    
    //If the player wasn't grounded last frame and is grounded this frame and last velocity y is less than -13, then generate paritcles around player
    if (!lastGrounded && grounded && lastVY < -13) {
        level.addParticle(x, y-5, 15, 90+10+random(5)-2.5, 8, true, false, this);
        level.addParticle(x, y-5, 15, 90+20+random(5)-2.5, 8, true, false, this);
        level.addParticle(x, y-5, 15, 90+30+random(5)-2.5, 8, true, false, this);
        level.addParticle(x+size, y-5, 15, 90-10+random(5)-2.5, 8, true, false, this);
        level.addParticle(x+size, y-5, 15, 90-20+random(5)-2.5, 8, true, false, this);
        level.addParticle(x+size, y-5, 15, 90-30+random(5)-2.5, 8, true, false, this);
        Sound.LANDING.play();
    }
    
    //Sets last grounded and last velocity y to this frames variables
    lastGrounded = grounded;
    lastVY = vy;
    
    //When the player has jumped it sets jump to false
    jump = false;
    
    //Moves the player
    move();
  }
  
  //Checks if Entity collides with another Entity
  @Override
  public boolean collides(Entity e, float xOffs, float yOffs) {
    boolean collides = super.collides(e, xOffs, yOffs);
    
    if (e instanceof Particle) return false;
    
    return collides;
  }
  
  //This method gets called if the player is out of bounds
  public void oob() {
    level.levelReset = true;
  }
  
  //Resets the player
  public void reset() {
    vx = vy = 0;
    x = y = 300;
    jump = false;
    xDirection = 0;
    onWall = 0;
    grounded = false;
  }
  
  public void render() {
    //Rendering the player
    fill(255, 0, 255);
    
    //If pleyer dies, then play anoyher animation
    if (level.levelReset) {
      //Push the matrix, so it can me deleted
      pushMatrix();
      
      //If reset animation has run for 20 out of 30 frames, then move player
      if (level.resetAnimation >= 20) {
        //Creates a vector from player to player's starting position and moves the player with 15% of that vector
        x += (level.playerX-x)*0.15;
        y += (level.playerY-y)*0.15;
      }
      
      //Offsets the position by where the player needs to be rendered plus half of height
      translate((int) x+16, height-(int) y-size+16);
      //Rotates player potentially
      rotate(0.002*pow(level.resetAnimation,2.3));
      //Draws player, but with an offset og 16
      rect(-16, -16, 32, 32);
      //Deletes everything
      popMatrix();
    } else
      rect((int) x, height-(int) y-size, 32, 32);
  }
}
