public class Title {
  boolean inIntro = true;
  int selected;
  
  public void render() {
    //If in intro of the title, then run special loop, else execute the normal loop
    if (inIntro) {
      //Creates a string, with the destination to the title intro
      String str = "Title Intro/IntroTitle";
      //Adds zeroes based on the number, log((int) titleScreenFrameCount)/log(10) returns how many zeroes there already is,
      //subtracting two from it, returns how many zeroes is needed for it to have three digits
      for (int i = 0; i < ((int) titleScreenFrameCount==0?2:(2-log((int) titleScreenFrameCount)/log(10))); i++) {
        str += 0;
      }
      //Adds the number and ".png" to get the destination of the frame needed
      str += (int) titleScreenFrameCount+".png";
      //Renders current frame
      image(loadImage(str), 0, 0);
    } else {
      //Creates a string, with the destination to the title intro
      String str = "Title/Title"+(((int) titleScreenFrameCount)-104)%8+".png";
      //Renders current frame
      image(loadImage(str), 0, 0);
    }
    
    //Different speeds for differant frames
    //The first 10 frames is the First row of the logo
    if (titleScreenFrameCount < 10)
      titleScreenFrameCount += 1;
    
    //We want to pause after the first frames, so we can see the title
    else if ((int) titleScreenFrameCount == 10)
      titleScreenFrameCount += 0.04;
    
    //We want to pause the animation a little befire the fire on the letters begins burning
    else if ((int) titleScreenFrameCount == 36)
      titleScreenFrameCount += 0.05;
    
    //We want the animation to go slower after the big burn
    else if (titleScreenFrameCount < 38)
      titleScreenFrameCount += 0.4;
    else
      titleScreenFrameCount += 0.25;
      
    //Add sound effect
    //Play explosion sound when "SUPER ULTIMATE JUMP BOX! 64:" hits screen
    if (titleScreenFrameCount == 10)
      Sound.TITLE_EFFECT_EXPLOSION.play();
    
    //Play loop of burning sound effect, when "TOURNAMENT EDITION" appears
    if ((int) titleScreenFrameCount == 11)
      Sound.TITLE_EFFECT_BURN_LONG.loop();
    
    //Stops loop when it isn't burning anymore
    if ((int) titleScreenFrameCount == 31)
      Sound.TITLE_EFFECT_BURN_LONG.stop();
      
    //Loops quiter version of burning sound effect when the small fire on the letters appear
    if ((int) titleScreenFrameCount == 37)
      Sound.TITLE_EFFECT_BURN.loop();
    
    //Fade burning sound effect out, when "TOURNAMENT EDITION" is finished appearing
    if ((int) titleScreenFrameCount == 45)
      Sound.TITLE_EFFECT_BURN.volume(17);
    if ((int) titleScreenFrameCount == 46)
      Sound.TITLE_EFFECT_BURN.volume(14);
    if ((int) titleScreenFrameCount == 47)
      Sound.TITLE_EFFECT_BURN.volume(11);
    if ((int) titleScreenFrameCount == 48)
      Sound.TITLE_EFFECT_BURN.volume(8);
    if ((int) titleScreenFrameCount == 49)
      Sound.TITLE_EFFECT_BURN.volume(5);
      
    //Plays & Knuckles title theme sound
    if ((int) titleScreenFrameCount == 85)
      Sound.TITLE_AND_KNUCKLES.play();
    
    //Turns down the burning even more, when the title is done appearing
    if ((int) titleScreenFrameCount == 104)
      Sound.TITLE_EFFECT_BURN.volume(2);
    
    //Plays a shooting like sound, everytime a letter appears 
    if ((((int) titleScreenFrameCount)) > 50 && (((int) titleScreenFrameCount)) < 81 && (((int) titleScreenFrameCount)-50)%2 == 0)
      Sound.TITLE_LEATTER_APPEARING.play();
    
    //If there isn't anymore intro, then loop back to the beginning
    if (titleScreenFrameCount > 104) inIntro = false;
    
    if (!inIntro) {
      //Renders options when the intro isn't playing

      fill(0);
      textSize(45);
      text("Play", width/2-textWidth("Play")/2, 340);
      text("Exit", width/2-textWidth("Exit")/2, 340+60);
      
      //Render selecter based on what is selected
      text("->", width/2-textWidth("->")-10-textWidth("Play")/2, 340+60*(selected%2));
    }
  }
  
  public void select() {
    //If in intro and player presses space, then skip intro scene
    if (inIntro) {
      titleScreenFrameCount = 104;
      inIntro = false;
      Sound.TITLE_EFFECT_BURN.stop();
      Sound.TITLE_EFFECT_BURN_LONG.stop();
      Sound.TITLE_EFFECT_EXPLOSION.stop();
      Sound.TITLE_LEATTER_APPEARING.stop();
      Sound.TITLE_EFFECT_BURN.volume(2);
      Sound.TITLE_EFFECT_BURN.loop();
      return;
    }
    
    //If selected is play, then set onTitle to false
    if (selected%2 == 0) {
      onTitle = false;
      Sound.SELECT.play();
      Sound.TITLE_EFFECT_BURN.stop();
      Sound.TITLE_EFFECT_BURN_LONG.stop();
      Sound.TITLE_EFFECT_EXPLOSION.stop();
      Sound.TITLE_LEATTER_APPEARING.stop();
    }
    //If selected is exit, then exit game
    else
      System.exit(0);
  }
}