import javax.sound.sampled.*;
import java.io.BufferedInputStream;

public static class Sound {
  public static Sound TITLE_EFFECT_EXPLOSION = loadSound("Title Effect Explosion 1.wav", 60);
  public static Sound TITLE_EFFECT_BURN = loadSound("Title Effect Burning 4.wav", 60);
  public static Sound TITLE_EFFECT_BURN_LONG = loadSound("Title Effect Burning 3.wav", 60);
  public static Sound TITLE_LEATTER_APPEARING = loadSound("Title Letter Appearing 4.wav", 70);
  public static Sound TITLE_AND_KNUCKLES = loadSound("& Knuckles.wav", 70);
  public static Sound JUMP_SOUND = loadSound("Jump 1.wav", 65);
  public static Sound SELECT = loadSound("Select 1.wav", 60);
  public static Sound GOAL = loadSound("Goal 1.wav", 60);
  public static Sound ENEMY_HIT = loadSound("Enemy Hit 4.wav", 80);
  public static Sound WALL_JUMP = loadSound("Wall Jump 1.wav", 65);
  public static Sound LANDING = loadSound("Landing 1.wav", 60);
  public static Sound CRUMBLING_BLOCK = loadSound("Crumbling Block 2.wav", 60);
  
  public static Sound loadSound(String fileName, int volume) {
    Sound sound = new Sound();
    sound.volume = volume;
    try {
      AudioInputStream ais = AudioSystem.getAudioInputStream(new BufferedInputStream(please_no_steal.createInput("Sounds/"+fileName)));
      Clip clip = AudioSystem.getClip();
      clip.open(ais);
      sound.clip = clip;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    
    return sound;
  }
  
  public static Sound loadSound(String fileName) {
    return loadSound(fileName, 100);
  }

  private Clip clip;
  private float volume;
  private int lastPlayFrame;
  
  public void play() {
    if (lastPlayFrame == please_no_steal.frameCount) return;
    lastPlayFrame = please_no_steal.frameCount;
    
    try {
      new Thread() {
        public void run() {
          synchronized (clip) {
            clip.stop();
            clip.setFramePosition(0);
            
            FloatControl volumeControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            volumeControl.setValue((volumeControl.getMaximum() - volumeControl.getMinimum()) / 100.0f * volume + volumeControl.getMinimum());
            
            clip.start();
          }
        }
      }.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public void loop() {
    try {
      new Thread() {
        public void run() {
          synchronized (clip) {
            clip.stop();
            clip.setFramePosition(0);
            
            FloatControl volumeControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            volumeControl.setValue((volumeControl.getMaximum() - volumeControl.getMinimum()) / 100.0f * volume + volumeControl.getMinimum());
            
            clip.loop(Clip.LOOP_CONTINUOUSLY);
          }
        }
      }.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void stop() {
    clip.stop();
  }
  
  public void volume(float volume) {
    this.volume = volume;
  }
}

//Old code
/*
import processing.sound.*;

public static class Sound {
  //List of all sounds in the game

  public static Sound TITLE_EFFECT_EXPLOSION = new Sound("Title Effect Explosion 1.wav");
  public static Sound TITLE_EFFECT_BURN = new Sound("Title Effect Burning 4.wav");
  public static Sound TITLE_EFFECT_BURN_LONG = new Sound("Title Effect Burning 3.wav", 20);
  public static Sound TITLE_LEATTER_APPEARING = new Sound("Title Letter Appearing 4.wav", 20);
  public static Sound JUMP_SOUND = new Sound("Jump 1.wav", 40);
  public static Sound SELECT = new Sound("Select 1.wav", 80);
  public static Sound GOAL = new Sound("Goal 1.wav", 60);
  public static Sound ENEMY_HIT = new Sound("Enemy Hit 3", 20);

  //Variable to store information about the sounds
  private SoundFile sound;
  private String path;
  private boolean loops;
  
  Sound(String file) {
    //If the sound doesn't have a ".wav" at the end, then add it
    if (!file.endsWith(".wav")) file += ".wav";
    
    path = "Sounds/"+file;
    
    sound = new SoundFile(please_no_steal, path);
  }
  
  Sound(String file, int volumen) {
    this(file);
    volumen(volumen);
  }
  
  //Plays the sound on a different thread, to avoid lag from loading
  public void play() {
    new Thread(new Runnable() {
      public void run() {
        sound.stop();
        sound.play();
      }
    }).start();
  }
  
  public void loop() {
    if (loops) return;
    
    loops = true;
    
    new Thread(new Runnable() {
      public void run() {
        sound.loop();
      }
    }).start();
  }
  
  public void stop() {
    loops = false;
    
    sound.stop();
  }
  
  public void volumen(float volumen) {
    sound.amp(volumen / 100.0);
  }
}*/
