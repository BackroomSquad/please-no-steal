public class GoalBlock extends Block {
  
  //Gives the goal block an x and y value
  public GoalBlock (int x, int y){
    super (x,y);
    solid = false;
  }
  
  public void update() {
  }
  
  public boolean collides(Entity e) {
    if (e instanceof Player) {
      Sound.GOAL.play();
      level.levelLoading = true;
    }
    
    return super.collides(e);
  }
  
  public void render() {
    //Renders the goal block
    fill(255, 215, 0);
    
    rect(x*32, (int) height-y*32-32, 32, 32);
  }
}
