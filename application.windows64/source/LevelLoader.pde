public class LevelLoader {
  public boolean loadLevel(Level level, String str) {
    if (!str.endsWith(".png")) str += ".png";
    
    //If the level doesn't exist, then return false
    if (!new File(sketchPath()+"/data/"+str).exists()) return false;
    
    //Loads level image
    PImage levelImage = loadImage(str);
    
    //Generates pixel array from level image
    levelImage.loadPixels();
    
    //Removes all entites from the level
    level.entities.clear();
    
    //Loops through level
    for (int y = 0; y < level.lvHeight; y++) {
      for (int x = 0; x < level.lvWidth; x++) {
        //Resets all the blocks back to noting  
        level.blocks[x+(level.lvHeight-y-1)*level.lvWidth] = new Block(x, level.lvHeight-y-1);
        
        int col = levelImage.pixels[x+y*levelImage.width];
        
        //Sets blocks according to color of pixels
        switch (col & 0xffffff | 0xff000000) {
          //Black pixel is SolidBlock
          case #000000:
            level.blocks[x+(level.lvHeight-y-1)*level.lvWidth] = new SolidBlock(x, level.lvHeight-y-1);
            break;
          
          //Red pixel is player position
          case #FF0000:
            level.player.reset();
            level.player.x = x*32;
            level.player.y = (level.lvHeight-y-1)*32;
            level.playerX = x*32;
            level.playerY = (level.lvHeight-y-1)*32;
            level.addEntity(level.player);
            break;
          
          //Blue pixel loads enemy
          case #0000FF:
            level.addEntity(new WalkingEnemy(x*32, (level.lvHeight-y-1)*32, ((col>>24)&0xFF)>128 ? -1 : 1));
            break;
            
          //Yellow pixel is GoalBlock
          case #FFFF00:
            level.blocks[x+(level.lvHeight-y-1)*level.lvWidth] = new GoalBlock(x, level.lvHeight-y-1);
            break;
            
         //Green pixel is a crumbling block
         case #00FF00:
           level.blocks[x+(level.lvHeight-y-1)*level.lvWidth] = new CrumblingBlock(x, level.lvHeight-y-1, (col>>24)&0xFF);
           break;
        }
      }
    }
    
    return true;
  }
  
  //Loads next level, based on a levelID
  public boolean loadlevel(Level level, int levelID) {
    return loadLevel(level, "Level"+levelID);
  }
}
