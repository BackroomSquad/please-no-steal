[33mcommit ed937321ff537b8cdd42e860bc8ecfd5607e2a46[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Wed Nov 7 12:22:52 2018 +0100

    Added timer

[33mcommit d313ffdca33861f68d6cba86f9838cf20911b04e[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Wed Nov 7 15:49:05 2018 +0100

    Added death animation

[33mcommit 9b869de6911ed9a69111af4ebd0543af3315b271[m
Merge: 193eb62 bd7a4be
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Wed Nov 7 11:39:05 2018 +0100

    Merge branch 'master' of gitlab.com:BackroomSquad/please-no-steal

[33mcommit 193eb62a3a867eab1100e02086cd7466781e2bab[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Wed Nov 7 11:38:57 2018 +0100

    Fixed entity collision with other entities

[33mcommit bd7a4beb204c6a7d35e1aec67a8038559d8957c4[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Wed Nov 7 10:09:16 2018 +0100

    Added more to the report

[33mcommit 15e15f275b832adaeeeddaaf9a7a9486ade86e30[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Wed Nov 7 09:58:45 2018 +0100

    Added more particles and fixed sound

[33mcommit 1eb849263973916bcdb9fd6c4fed91a1ccb09d0a[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Tue Nov 6 22:26:36 2018 +0100

    Fixed sound

[33mcommit 351b2fa7c05cf392d122381a2445528ce4980ed8[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Tue Nov 6 21:16:18 2018 +0100

    [Not working] Updated sound system

[33mcommit 64d471b7c1635919a89793d3db04d70c73d984a3[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Tue Nov 6 16:14:51 2018 +0100

    Added sound effects

[33mcommit c3407bc800ba4124a8aba7bef1c0a66d200da572[m
Merge: 1613067 c06d350
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Tue Nov 6 15:18:27 2018 +0100

    Merge branch 'master' of gitlab.com:BackroomSquad/please-no-steal

[33mcommit 16130678df37daeed511f44edfad55f7c47cf8c2[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Tue Nov 6 15:18:14 2018 +0100

    Added particles

[33mcommit c06d35053be1abbefadb7052663e2eaabbb36aaa[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Tue Nov 6 14:55:54 2018 +0100

    Added crumble effect to CrumblingBlocks when player has touched it

[33mcommit ab80167282366d9c40862ab78fc729b57f3169fd[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Tue Nov 6 14:03:27 2018 +0100

    Fixed TextLoader to not give error on last level win

[33mcommit cf708381aa8c1fc94411bae2dcaba926a49c5d2a[m
Merge: 91e0e39 e914ea9
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Tue Nov 6 13:43:49 2018 +0100

    Merge branch 'master' of https://gitlab.com/BackroomSquad/please-no-steal

[33mcommit e914ea9ee52908ec6e5189a292ee6a150d675919[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Mon Nov 5 13:24:49 2018 +0100

    Updated level and added flavor text

[33mcommit 91e0e396fa0fd381dbf4bab53806941425f41814[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Mon Nov 5 13:28:19 2018 +0100

    Updated level and added flavor text

[33mcommit c9ab37757c0c262a7c32109364ff2b9d556f557d[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Mon Nov 5 13:24:49 2018 +0100

    Updated level and added flavor text

[33mcommit da5fbeff88146310dfb5060a047c86098b971dcf[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Mon Nov 5 12:30:35 2018 +0100

    Updated entity collider

[33mcommit 11392c3f61983abb4493ae7e14a1c9c4208f1ba1[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Sat Nov 3 11:31:12 2018 +0100

    Fixed title screen music and entity collider

[33mcommit 4579882994ba986bcb49407eb396f4c11b7cad99[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Thu Nov 1 22:48:33 2018 +0100

    Added sound to title screen

[33mcommit 3330863c34d3c139479c1a7f64b459a8e00a11c1[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Thu Nov 1 20:56:39 2018 +0100

    Updated wall sliding and jumping

[33mcommit a5ccaa873cecce366be2d4591c84ecdbf782f9e0[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Thu Nov 1 19:52:26 2018 +0100

    Added title screen to game

[33mcommit 7b0b0012fdbcad879ad80a02ec9e661c2bc5d4ce[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Thu Nov 1 16:43:05 2018 +0100

    Finished title screen

[33mcommit 80d97e630a22a98047ef768932cc4b930e2bb3bf[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Wed Oct 31 22:17:44 2018 +0100

    Created title screen

[33mcommit 8d34db5e1cd02664689cbb40e1288952de407ecd[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Fri Nov 2 11:22:14 2018 +0100

    Added more levels and updated crumbling block

[33mcommit 78edb3bccf2e447f3008f49c0d5c434a1e1d73ac[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Wed Oct 31 21:55:10 2018 +0100

    Added the crumbling block, levels and fixed reset after win

[33mcommit 7b04715ccac524659afbbe53db9c052e9b640d28[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Tue Oct 30 18:24:08 2018 +0100

    Added win screen, OOB death, and new level

[33mcommit 96714de68553bd56697a752781a1ef81c98cec09[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Tue Oct 30 17:00:29 2018 +0100

    Implemented walking enemies

[33mcommit f8ecd8e1be6272e59078b490d537bc276b7b524e[m
Merge: 2d2774a 7d3da9c
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Tue Oct 30 15:26:31 2018 +0100

    Merge branch 'master' of gitlab.com:BackroomSquad/please-no-steal

[33mcommit 2d2774ac2b39c9418b559a92b1a124294634eda6[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Tue Oct 30 15:26:25 2018 +0100

    Fixed bugs

[33mcommit 7d3da9cd6c4a82995c9277d7d52c4561ed6d55fc[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Tue Oct 30 10:12:50 2018 +0100

    Fixed player position on new level

[33mcommit 11569ddb549660898964034b2dec918f572e9c29[m
Merge: 7bfcb5e f743de1
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Tue Oct 30 09:11:06 2018 +0100

    Resolved merge conflict

[33mcommit 7bfcb5eae99ea39604585355cbc231fef1e03f05[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Tue Oct 30 09:05:36 2018 +0100

    Added the goal block and loading next level

[33mcommit f743de15415ad45809a6684897f695c67e2ffdc0[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Tue Oct 30 08:55:00 2018 +0100

    Added collider between entities

[33mcommit f275fec2548cb5d397ce71205abc660120ddb278[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Mon Oct 29 18:47:56 2018 +0100

    Bugs fixed and small updates
    
    Forgot to save

[33mcommit bf9a36c392901545c95608e11811a3d8ee3fffd4[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Mon Oct 29 18:21:52 2018 +0100

    Added collision code to block and updated level loading code

[33mcommit 4e668a58f3387b6686142b799b81958947b54958[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Mon Oct 29 16:21:59 2018 +0100

    Added comments, wall jumping, and drag

[33mcommit d68837d1871467c6614d885c95ded4ac25ca9f94[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Mon Oct 29 15:05:15 2018 +0100

    Added comments and goal block

[33mcommit 81c13a6cea88d4a0aa61b6801505e9d2933508cb[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Mon Oct 29 13:05:57 2018 +0100

    Tweaked player speed

[33mcommit 2348fe86b52abe2444bf73db67542ba3a24c7be3[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Sat Oct 27 19:58:37 2018 +0200

    Red is now setting player spawn

[33mcommit 21a72456f1d444e4ebcfbd63b618cace22d32958[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Sat Oct 27 19:44:56 2018 +0200

    Fixed entity collider

[33mcommit 2f7aac819e60ea1d4a19bb1e30ee055a311dc702[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Sat Oct 27 12:34:21 2018 +0200

    Created a level loader

[33mcommit 4bace5f1a42ec162c71d61f42943261d3416e214[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Fri Oct 26 10:36:44 2018 +0200

    Fixed player

[33mcommit bebf9a2f93c28e859a765f1503939c162e8050da[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Fri Oct 26 10:34:11 2018 +0200

    Updated player to only jump on ground

[33mcommit d8b0b2211fe952a1fe63ba51260caf7c8e9b69b9[m
Author: Jonas Jensen <jdudal11@gmail.com>
Date:   Fri Oct 26 10:29:18 2018 +0200

    Created Entity class and updated collision

[33mcommit d11be2fe763416e650dfd47524a28641c1902db1[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Thu Oct 25 15:55:14 2018 +0200

    Updated player and created level

[33mcommit c8ef2b4d33231c0d3c9cee2ce2d68b54cc8074bf[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Tue Oct 23 15:29:20 2018 +0200

    Setup player

[33mcommit f12bea184c077f644a5d7369d6c29bad737bd315[m
Author: Lene Christensen <lene.hojberg.christensen@gmail.com>
Date:   Tue Oct 23 11:28:09 2018 +0200

    Initial Commit
