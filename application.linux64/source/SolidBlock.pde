public class SolidBlock extends Block {
  public SolidBlock(int x, int y) {
    super(x, y);
    //Adds collition to the block
    solid = true;
  }
  
  public void render() {
    fill(255);
    
    rect(x*32, (int) height-y*32-32, 32, 32);
  }
}
