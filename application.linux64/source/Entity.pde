public class Entity {
  //Standard variables all entities must have
  public float x, y;
  public float vx, vy;
  public boolean grounded;
  public int onWall;
  public float speed = 1;
  public float size = 32;
  
  public Entity(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  //This code tries to move the entity based on the given velocity x (vx) and velocity y (vy) 
  protected void move() {
    float moveX = 0, moveY = 0;
    
    //Loops through numbers 100-1 and converts it into percent, and multplies it with the velocity
    for (int i = 100; i > 0; i--) {
      float vpy = vy/100.0*i;
      
      //If the entity doesn't collide after getting move, the percentage amount, then move the play to that point and break
      if (!collides(0, vpy)) {
        moveY = vpy;
        break;
      }
    }
    
    //If the entity hits something in the y direction, then set vy to zero
    grounded = false;
    if (collides(0, vy)) {
      //If the direction of the player is negative, then the player is grounded
      if (vy < 0) grounded = true;
      vy = 0;
    }
    
    //Moves entity to avoid collission error
    y += moveY;
    
    for (int i = 100; i > 0; i--) {
      float vpx = (vx*speed)/100.0*i;
      
      if (!collides(vpx, 0)) {
        moveX = vpx;
        break;
      }
    }
    
    //If the entity hits something in the x direction, then set vx to zero
    if (collides(vx*speed, 0)) {
      //Set onWall to ether 1 or -1 based on if it is left or right to the entity
      onWall = (int) (-vx*speed / abs(vx*speed));
    } else onWall = 0;
    
    //Move the entity
    x += moveX;
    
    //Applies drag to entity on ground (0.6) and in air (0.9)
    if (grounded) vx *= 0.6;
    else vx *= 0.9;
  }
  
  //Checks if the entity collides with a block after moving vx and vy in x and y
  public boolean collides(float vx, float vy) {
    //Gets the blocks on the bottom left, bottom right, top left, top right of the entity
    Block bl = level.getBlock((int) (x+vx       )/32, (int) (y+vy       )/32);
    Block br = level.getBlock((int) (x+vx+size-1)/32, (int) (y+vy       )/32);
    Block tl = level.getBlock((int) (x+vx       )/32, (int) (y+vy+size-1)/32);
    Block tr = level.getBlock((int) (x+vx+size-1)/32, (int) (y+vy+size-1)/32);
   
    //If any of the blocks are out of bounds, then return true
    if (bl == null || br == null || tl == null || tr == null) {
      oob();
      return true;
    }
    
    boolean collidesEntity = false;
    
    //Goes through all enteties and if the entity is colliding with this, then set collidesEntity to true
    for (Entity e : level.entities) {//level.getEntityWithinAABB(x,y,32,32)) {
      if (e.collides(this, vx, vy) && collides(e, -vx, -vy)) {
        collidesEntity = true;
      }
    }
    
    //Checks if it collieds with any of the blocks, this also ads a collider mothod to blocks
    boolean blc = bl.collides(this);
    boolean brc = br.collides(this);
    boolean tlc = tl.collides(this);
    boolean trc = tr.collides(this);
    return blc || brc || tlc || trc || collidesEntity;
  }
  
  //Checks if Entity collides with another Entity
  public boolean collides(Entity e, float xOffs, float yOffs) {
    if (e == this) return false;
    
    if (e.x+xOffs < x+size && e.x+xOffs+e.size > x && e.y+yOffs < y+size && e.y+yOffs+e.size > y) {
      return true;
    }
    
    return false;
  }
  
  //This code gets run when the entity is out of bounds
  public void oob() {
  }
  
  //Standard update code, this will get run on all entities
  public void update() {
  }
  
  //Standard render code, this will get run on all entities
  public void render() {
  }
}
