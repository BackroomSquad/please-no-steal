public class WalkingEnemy extends Enemy {
  private int direction;
  
  public WalkingEnemy(float x, float y, int dir) {
    super(x, y);
    
    direction = dir;
  }
  
  public void update() {
    //If the enemy hist a wall, then turn around
    if (onWall != 0) direction = onWall;
    
    //Moves the enemy in the x dirrection based on direction
    vx = direction;
    
    //Adds gravity to enemy
    vy += level.gravity;
    
    //Moves the enemy
    move();
  }
  
  public void render() {
    //Rendering the player
    fill(0, 0, 255);
    
    rect((int) x, height-(int) y-size, 32, 32);
  }
}
