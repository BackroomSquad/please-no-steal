public class Block {
  public int x, y;
  
  protected boolean solid;
  
  public Block(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public void update() {
  }
  
  public void render() {
  }
  
  //Returns wether or not the block is solid
  public boolean isSolid() {
    return solid;
  }
  
  //Returns true if the entity collides with the entity
  public boolean collides(Entity e) {
    return isSolid();
  }
}
