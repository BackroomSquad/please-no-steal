import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import javax.sound.sampled.*; 
import java.io.BufferedInputStream; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class ULTIMATE_JUMP_BOX extends PApplet {

Level level;
float titleScreenFrameCount;
boolean onTitle = true;
Title title = new Title();
static PApplet please_no_steal;

public void setup() {
  //Sets the size of the window, the stroke colour and the strokeWeight
  
  stroke(255);
  strokeWeight(0);
  surface.setTitle("SUPER ULTIMATE JUMP BOX! 64: TOURNAMENT EDITION 2000x CLIMAX REMIX & KNUCKLES");
  
  level = new Level();
  
  please_no_steal = this;
  
  //Loads level for testing
  level.loadNextLevel();
}

private int lastFrameCount;
private long lastTimeNano = System.nanoTime();
public void draw() {
  //FPS
  if (System.nanoTime()-lastTimeNano >= 1000000000L) {
    println((frameCount-lastFrameCount)+"FPS");
    lastTimeNano = System.nanoTime();
    lastFrameCount = frameCount;
  }
  
  //If on title, then run different code
  if (onTitle) {
    title.render();
  } else {
    //Updating player and other code that needs it
    level.update();
    
    //Renderering screen 
    background(0);
    level.render();
  }
}

//Tests if different keys are pressed or not and sets variables accordingly.
public void keyPressed() {
  if (keyCode == RIGHT || key == 'd') {
    level.player.xDirection = 1;
  }
  
  if (keyCode == LEFT || key == 'a') {
    level.player.xDirection = -1;
  }
  
  if (keyCode == UP || keyCode == DOWN) title.selected++;
  
  if (keyCode == DOWN) level.player.slideDown = true; 
  
  if (key == ' ') {
    if (onTitle) {
      title.select();
    } else {
      level.player.jump = true;
    }
  }
  
  if (key == 'r') level.levelReset = true;
}

public void keyReleased() {
  if (keyCode == RIGHT || key == 'd') {
    level.player.xDirection = 0;
  }
  
  if (keyCode == LEFT || key == 'a') {
    level.player.xDirection = 0;
  }
  
  if (keyCode == DOWN) level.player.slideDown = false;
}
public class Block {
  public int x, y;
  
  protected boolean solid;
  
  public Block(int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public void update() {
  }
  
  public void render() {
  }
  
  //Returns wether or not the block is solid
  public boolean isSolid() {
    return solid;
  }
  
  //Returns true if the entity collides with the entity
  public boolean collides(Entity e) {
    return isSolid();
  }
}
public class CrumblingBlock extends Block {
  private int timer = 0;
  private boolean touched;
  private float vy;
  private float yFall;
  private int fallFrames;
  private ArrayList<int[]> crumbles = new ArrayList<int[]>();
 
  public CrumblingBlock(int x, int y, int fallFrames) {
    super(x, y);
    this.fallFrames = fallFrames;
    //Adds collition to the block
    solid = true;
  }
  
  public boolean collides(Entity e) {
     if (e instanceof Player) {
         touched = true;
    }
    
    return super.collides(e);
    
  }
  
  public void update() {
    if (touched) {
      timer++;
    }
    if (timer > fallFrames) {
      solid = false;
      vy += level.gravity;
      yFall += vy;
    }
    
    if ((float)timer/fallFrames > 0.1f && crumbles.size() == 0) {
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      Sound.CRUMBLING_BLOCK.play();
    }
    
    if ((float)timer/fallFrames > 0.4f && crumbles.size() == 1) {
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
    }
    
    if ((float)timer/fallFrames > 0.9f && crumbles.size() == 4) {
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      crumbles.add(new int[] {(int)random(32-5),(int)random(32-5)});
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      level.addParticle(x*32+16, y*32+16, 60, 270+random(90)-45, 10, false, false, null);
      Sound.CRUMBLING_BLOCK.play();
    }
  }
  
  public void render() {
    fill(0);
    strokeWeight(3);
    stroke(255);
    
    rect(x*32, (int) height-y*32-32-yFall, 32, 32);
    noStroke();
    
    fill(255);
    for(int[] pos : crumbles) {
      rect(x*32+pos[0], height-y*32-32-yFall+pos[1], 5, 5);
    }
  }
}
public class Enemy extends Entity {
  public Enemy(float x, float y) {
    super(x, y);
  }
  
  //Checks if Entity collides with another Entity
  @Override
  public boolean collides(Entity e, float xOffs, float yOffs) {
    boolean collides = super.collides(e, xOffs, yOffs);
    
    //If enetity collides and it is with player, then reset level
    if (collides && e instanceof Player && !level.levelReset) {
      Sound.ENEMY_HIT.stop();
      Sound.ENEMY_HIT.play();
      level.levelReset = true;
    }
    
    return collides;
  }
}
public class Entity {
  //Standard variables all entities must have
  public float x, y;
  public float vx, vy;
  public boolean grounded;
  public int onWall;
  public float speed = 1;
  public float size = 32;
  
  public Entity(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  //This code tries to move the entity based on the given velocity x (vx) and velocity y (vy) 
  protected void move() {
    float moveX = 0, moveY = 0;
    
    //Loops through numbers 100-1 and converts it into percent, and multplies it with the velocity
    for (int i = 100; i > 0; i--) {
      float vpy = vy/100.0f*i;
      
      //If the entity doesn't collide after getting move, the percentage amount, then move the play to that point and break
      if (!collides(0, vpy)) {
        moveY = vpy;
        break;
      }
    }
    
    //If the entity hits something in the y direction, then set vy to zero
    grounded = false;
    if (collides(0, vy)) {
      //If the direction of the player is negative, then the player is grounded
      if (vy < 0) grounded = true;
      vy = 0;
    }
    
    //Moves entity to avoid collission error
    y += moveY;
    
    for (int i = 100; i > 0; i--) {
      float vpx = (vx*speed)/100.0f*i;
      
      if (!collides(vpx, 0)) {
        moveX = vpx;
        break;
      }
    }
    
    //If the entity hits something in the x direction, then set vx to zero
    if (collides(vx*speed, 0)) {
      //Set onWall to ether 1 or -1 based on if it is left or right to the entity
      onWall = (int) (-vx*speed / abs(vx*speed));
    } else onWall = 0;
    
    //Move the entity
    x += moveX;
    
    //Applies drag to entity on ground (0.6) and in air (0.9)
    if (grounded) vx *= 0.6f;
    else vx *= 0.9f;
  }
  
  //Checks if the entity collides with a block after moving vx and vy in x and y
  public boolean collides(float vx, float vy) {
    //Gets the blocks on the bottom left, bottom right, top left, top right of the entity
    Block bl = level.getBlock((int) (x+vx       )/32, (int) (y+vy       )/32);
    Block br = level.getBlock((int) (x+vx+size-1)/32, (int) (y+vy       )/32);
    Block tl = level.getBlock((int) (x+vx       )/32, (int) (y+vy+size-1)/32);
    Block tr = level.getBlock((int) (x+vx+size-1)/32, (int) (y+vy+size-1)/32);
   
    //If any of the blocks are out of bounds, then return true
    if (bl == null || br == null || tl == null || tr == null) {
      oob();
      return true;
    }
    
    boolean collidesEntity = false;
    
    //Goes through all enteties and if the entity is colliding with this, then set collidesEntity to true
    for (Entity e : level.entities) {//level.getEntityWithinAABB(x,y,32,32)) {
      if (e.collides(this, vx, vy) && collides(e, -vx, -vy)) {
        collidesEntity = true;
      }
    }
    
    //Checks if it collieds with any of the blocks, this also ads a collider mothod to blocks
    boolean blc = bl.collides(this);
    boolean brc = br.collides(this);
    boolean tlc = tl.collides(this);
    boolean trc = tr.collides(this);
    return blc || brc || tlc || trc || collidesEntity;
  }
  
  //Checks if Entity collides with another Entity
  public boolean collides(Entity e, float xOffs, float yOffs) {
    if (e == this) return false;
    
    if (e.x+xOffs < x+size && e.x+xOffs+e.size > x && e.y+yOffs < y+size && e.y+yOffs+e.size > y) {
      return true;
    }
    
    return false;
  }
  
  //This code gets run when the entity is out of bounds
  public void oob() {
  }
  
  //Standard update code, this will get run on all entities
  public void update() {
  }
  
  //Standard render code, this will get run on all entities
  public void render() {
  }
}
public class GoalBlock extends Block {
  
  //Gives the goal block an x and y value
  public GoalBlock (int x, int y){
    super (x,y);
    solid = false;
  }
  
  public void update() {
  }
  
  public boolean collides(Entity e) {
    if (e instanceof Player) {
      Sound.GOAL.play();
      level.levelLoading = true;
    }
    
    return super.collides(e);
  }
  
  public void render() {
    //Renders the goal block
    fill(255, 215, 0);
    
    rect(x*32, (int) height-y*32-32, 32, 32);
  }
}
public class Level {
  //Initializes variables
  final int lvWidth = 20, lvHeight = 15;
  final float gravity = -0.7f;
  //Sets the player position to 300, 300
  Player player = new Player(300, 300);
  //Makes an array of blocks with the size of width*height of level
  Block[] blocks = new Block[lvWidth*lvHeight];
  //Initializes LevelLoader
  LevelLoader ll = new LevelLoader();
  private int currentLevelID;
  public boolean levelLoading;
  public boolean levelReset;
  //Initializes list of Entities
  public ArrayList<Entity> entities = new ArrayList<Entity>();
  //Win variable, returns true if there is no more levels to be loaded
  public boolean win;
  private TextLoader tl;
  //Reset animation
  public int resetAnimation;
  public float playerX, playerY;
  //Timer
  private float timer;
  private float lastTime = -1;

  public Level() {
    //Adds player to list of entities
    addEntity(player);
    
    //Goes through all blocks and initialzes them
    for (int i = 0; i < blocks.length; i++) {
      int x = i%lvWidth;
      int y = i/lvWidth;
      blocks[i] = new Block(x, y);
    }
    
    tl = new TextLoader("LevelText.txt");
  }
  
  //Updates level
  public void update() {
    if (lastTime == -1) lastTime = System.nanoTime()/1000000000.0f;
    //If player has won, then don't update anything
    if (win) {
      //If the player hits the reset button, then set the level ID to 0 and load next level
      if (levelReset) {
        timer = 0;
        lastTime = System.nanoTime()/1000000000.0f;
        currentLevelID = 0;
        loadNextLevel();
        levelReset = false;
      }
      
      return;
    }
    
    //If levelReset is true, then add one to resetAnimation, and if that gets over 30, then animation is over and level should run normal code 
    if (levelReset) {
      resetAnimation++;
      
      if (resetAnimation <= 40) return;
      resetAnimation = 0;
    }

    timer += (System.nanoTime()/1000000000.0f)-lastTime;
    lastTime = (System.nanoTime()/1000000000.0f);
    
    //If levelLoad is pending, then load level, this is done to avoid errors form loading new level
    if (levelLoading) {
      loadNextLevel();
      levelLoading = false;
    }
    
    //If levelResset is pending, then reset level
    if (levelReset) {
      reset();
      levelReset = false;
    }
    
    //Updates all entites
    for (int i = 0; i < entities.size(); i++) {
      Entity e = entities.get(i);
      e.update();
      
      if (e instanceof Particle && ((Particle) e).destroy) entities.remove(i--);
    }
    
    //Updates all blocks
    for (Block b : blocks) {
      b.update();
    }
  }
  
  //Returns specific block based on x and y coordinates
  public Block getBlock(int x, int y) {
    //If the coordinate is out of bounds, return null
    if (x < 0 || y < 0 || x >= lvWidth || y >= lvHeight) return null;
    
    return blocks[x+y*lvWidth];
  }
  
  public void addEntity(Entity e) {
    entities.add(e);
  }
  
  //Returns list of entities within a AABB (Axis-Alligned Bounding Box)
  public ArrayList<Entity> getEntityWithinAABB(float x, float y, float w, float h) {
    ArrayList<Entity> result = new ArrayList<Entity>();
    
    for (Entity e : entities) {
      if (e.x > x && e.x < w && e.y > y && e.y < h) result.add(e);
    }
    
    return result;
  }
  
  public void render() {
    //Renders all entites
    for (Entity e : entities) {
      e.render();
    }
    
    //Goes through all blocks and calls their render
    for (int i = 0; i < blocks.length; i++) {
      blocks[i].render();
    }
    
    //Renderes flavor text at the top of the level
    fill(0);
    textSize(20);
    String flavorText = tl.getString(currentLevelID);
    text(flavorText, width/2-textWidth(flavorText)/2, 32-5);
    
    int minute = (int)timer/60;
    float seconds = timer%60;
    text(minute+":"+(seconds<10?"0":"")+round(seconds*100)/100.0f, width-textWidth("0:00.00")-5, height-10);
    
    if (win) {
      //Here goes code to render win screen
      
      //Adds shadow by offsetting text
      fill(0);
      textSize(65);
      //Left
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2+1);
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2-0);
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2-1);
      text("You're winner", width/2-textWidth("You're winner")/2-2, height/2-2);
      
      //Second column
      text("You're winner", width/2-textWidth("You're winner")/2-1, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2-1, height/2-2);
      
      //Third column
      text("You're winner", width/2-textWidth("You're winner")/2-0, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2-0, height/2-2);
      
      //Fourth column
      text("You're winner", width/2-textWidth("You're winner")/2+1, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2+1, height/2-2);
      
      //Right
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2+2);
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2+1);
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2-0);
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2-1);
      text("You're winner", width/2-textWidth("You're winner")/2+2, height/2-2);
      
      //Renders winning text with gold color and font size 65
      fill(255, 215, 0);
      textSize(65);
      //Centers the text by rendering the text from the screen width/2 minus the text width/2
      text("You're winner", width/2-textWidth("You're winner")/2, height/2);
    }
    
    if (resetAnimation > 30) {
      fill(255, 255, 255, 255/5*(resetAnimation-30));
      rect(0, 0, width, height);
    }
  }
  
  //Loads next level, only if there isn't a load currently going on
  public void loadNextLevel() {
    win = !ll.loadlevel(this, ++currentLevelID);
    levelLoading = false;
  }
  
  //Makes it easier to add a particle
  public void addParticle(float x, float y, int destroyTimer, float dirrection, float speed, Entity e) {
    entities.add(new Particle(x, y, destroyTimer, dirrection, speed, e));
  }
  
  //Makes it easier to add a particle
  public void addParticle(float x, float y, int destroyTimer, float dirrection, float speed, boolean useGravity, boolean destroyOnCollide, Entity e) {
    entities.add(new Particle(x, y, destroyTimer, dirrection, speed, useGravity, destroyOnCollide, e));
  }
  
  public void reset() {
    ll.loadlevel(this, currentLevelID);
  }
}
public class LevelLoader {
  public boolean loadLevel(Level level, String str) {
    if (!str.endsWith(".png")) str += ".png";
    
    //If the level doesn't exist, then return false
    if (!new File(sketchPath()+"/data/"+str).exists()) return false;
    
    //Loads level image
    PImage levelImage = loadImage(str);
    
    //Generates pixel array from level image
    levelImage.loadPixels();
    
    //Removes all entites from the level
    level.entities.clear();
    
    //Loops through level
    for (int y = 0; y < level.lvHeight; y++) {
      for (int x = 0; x < level.lvWidth; x++) {
        //Resets all the blocks back to noting  
        level.blocks[x+(level.lvHeight-y-1)*level.lvWidth] = new Block(x, level.lvHeight-y-1);
        
        int col = levelImage.pixels[x+y*levelImage.width];
        
        //Sets blocks according to color of pixels
        switch (col & 0xffffff | 0xff000000) {
          //Black pixel is SolidBlock
          case 0xff000000:
            level.blocks[x+(level.lvHeight-y-1)*level.lvWidth] = new SolidBlock(x, level.lvHeight-y-1);
            break;
          
          //Red pixel is player position
          case 0xffFF0000:
            level.player.reset();
            level.player.x = x*32;
            level.player.y = (level.lvHeight-y-1)*32;
            level.playerX = x*32;
            level.playerY = (level.lvHeight-y-1)*32;
            level.addEntity(level.player);
            break;
          
          //Blue pixel loads enemy
          case 0xff0000FF:
            level.addEntity(new WalkingEnemy(x*32, (level.lvHeight-y-1)*32, ((col>>24)&0xFF)>128 ? -1 : 1));
            break;
            
          //Yellow pixel is GoalBlock
          case 0xffFFFF00:
            level.blocks[x+(level.lvHeight-y-1)*level.lvWidth] = new GoalBlock(x, level.lvHeight-y-1);
            break;
            
         //Green pixel is a crumbling block
         case 0xff00FF00:
           level.blocks[x+(level.lvHeight-y-1)*level.lvWidth] = new CrumblingBlock(x, level.lvHeight-y-1, (col>>24)&0xFF);
           break;
        }
      }
    }
    
    return true;
  }
  
  //Loads next level, based on a levelID
  public boolean loadlevel(Level level, int levelID) {
    return loadLevel(level, "Level"+levelID);
  }
}
public class Particle extends Entity {
  public float direction;
  public float speed;
  public boolean effectedByGravity = true;
  public boolean destryOnCollide = true;
  public int destroyTimer;
  private int time;
  public boolean destroy;
  public Entity noCollision;
  
  //Simple constrcture
  public Particle(float x, float y, int destroyTimer, float direction, float speed, Entity noCollision) {
    super(x, y);
    
    this.direction = direction;
    this.speed = speed;
    this.noCollision = noCollision;
    this.destroyTimer = destroyTimer;
    
    vx = cos(radians(direction))*speed;
    vy = sin(radians(direction))*speed;
    
    size = 1;
  }
  
  //Specific constructer
  public Particle(float x, float y, int destroyTimer, float direction, float speed, boolean effectedByGravity, boolean destryOnCollide, Entity noCollision) {
    this(x, y, destroyTimer, direction, speed, noCollision);
    
    this.effectedByGravity = effectedByGravity;
    this.destryOnCollide = destryOnCollide;
  }
  
  @Override
  public void update() {
    //If the timer is greater than the destroyTimer, then destroy the particle
    if (time++ > destroyTimer)
      destroy();
    
    //If effectedByGravity is false, then add 0
    vy += effectedByGravity ? level.gravity : 0;
    
    //Moves particle
    move();
  }
  
  //Checks if Entity collides with another Entity
  @Override
  public boolean collides(float vx, float vy) {
    boolean col = super.collides(vx, vy);
    
    //If destryOnCollide is true, and particle collides, then destroy particle 
    if (destryOnCollide && col) {
      destroy();
    }
    
    return false;
  }
  
  //Checks if Entity collides with another Entity
  @Override
  public boolean collides(Entity e, float xOffs, float yOffs) {
    boolean col = super.collides(e, xOffs, yOffs);
    
    if (e instanceof Particle) return false;
    
    //If destryOnCollide is true, and particle collides, then destroy particle
    if (destryOnCollide && col) destroy();
    
    return col;
  }
  
  //Checks if Particle is out of bounds, then destroy
  @Override
  public void oob() {
    destroy();
  }
  
  public void destroy() {
    destroy = true;
  }
  
  public void render() {
    fill(255);
    stroke(255);
    strokeWeight(3);
    point(x, height-y-size);
    noStroke();
  }
}
public class Player extends Entity {
  //Initializes variables speficly for the player
  public boolean jump;
  public float jumpSpeed = 11;
  public float wallDashSpeed = 10;
  public int xDirection;
  public float maxVel = 1;
  public boolean slideDown;
  private int nextParticle = -1;
  private boolean lastGrounded;
  private float lastVY;
  
  public Player(float x, float y) {
    super(x, y);
    
    speed = 3;
  }
  
  public void update() {
    if (nextParticle-- <= -1) nextParticle = (int) random(25)+5;
    
    //xDirecton is equal to the button held down.
    //vx is equal to the direction the player is moving in.
    //If xDirection*vx is positive the player is pressing the same direction as they are moving
    //That value has to be less than maxVel, because we only want the player to manipulate the velocity if it is below maxVel
    if (xDirection * vx < maxVel)
      vx += xDirection;
    
    if (nextParticle == 0 && grounded && onWall == 0 && xDirection * vx >= maxVel*0.9f)
      level.addParticle(x-xDirection*(size/2+1)+size/2, y, 5, 90+xDirection*45, 10, this);
    
    //If player is on wall and player is moving in the same direction as the wall and player isn't trying to slide down and player doesn't move upwards, then reset vy, so player falls down slower
    if (onWall != 0 && onWall == -xDirection && !slideDown) {
      if (vy < 0) {
        vy = 0;
        if (nextParticle == 0) {
          if (onWall == 1) level.addParticle(x, y+size+5, 15, 80+random(20)-10, 5, this);
          else level.addParticle(x+size-5, y+size+5, 15, 90-80+90+random(20)-10, 5, this);
        }
      } else {
        if (nextParticle == 0) {
          if (onWall == 1) level.addParticle(x, y-5, 15, -80+random(20)-10, 5, this);
          else level.addParticle(x+size-5, y-5, 15, 90+80+90+random(20)-10, 5, this);
        }
      }
    }
    
    //Sets the players velocity based on gravity
    vy += level.gravity;
    
    //When the player is grounded and the jump button is pressed, the player accelerates up.
    //When the player is on a wall and not grounded and the jump button is pressed, the player jumps in the direction away from the wall and up.
    if (jump) {
      if (grounded) {
        vy = jumpSpeed;
        Sound.JUMP_SOUND.play();
      } else if (onWall != 0) {
        vy = jumpSpeed;
        vx = wallDashSpeed*onWall;
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90+10+random(5)-2.5f, 8, false, false, this);
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90+5+random(5)-2.5f, 8, false, false, this);
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90+random(5)-2.5f, 8, false, false, this);
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90-5+random(5)-2.5f, 8, false, false, this);
        level.addParticle(x+(onWall==-1?size-1:0), y+size/2, 15, 90+-onWall*90-10+random(5)-2.5f, 8, false, false, this);
        Sound.WALL_JUMP.play();
      }
    }
    
    //If the player wasn't grounded last frame and is grounded this frame and last velocity y is less than -13, then generate paritcles around player
    if (!lastGrounded && grounded && lastVY < -13) {
        level.addParticle(x, y-5, 15, 90+10+random(5)-2.5f, 8, true, false, this);
        level.addParticle(x, y-5, 15, 90+20+random(5)-2.5f, 8, true, false, this);
        level.addParticle(x, y-5, 15, 90+30+random(5)-2.5f, 8, true, false, this);
        level.addParticle(x+size, y-5, 15, 90-10+random(5)-2.5f, 8, true, false, this);
        level.addParticle(x+size, y-5, 15, 90-20+random(5)-2.5f, 8, true, false, this);
        level.addParticle(x+size, y-5, 15, 90-30+random(5)-2.5f, 8, true, false, this);
        Sound.LANDING.play();
    }
    
    //Sets last grounded and last velocity y to this frames variables
    lastGrounded = grounded;
    lastVY = vy;
    
    //When the player has jumped it sets jump to false
    jump = false;
    
    //Moves the player
    move();
  }
  
  //Checks if Entity collides with another Entity
  @Override
  public boolean collides(Entity e, float xOffs, float yOffs) {
    boolean collides = super.collides(e, xOffs, yOffs);
    
    if (e instanceof Particle) return false;
    
    return collides;
  }
  
  //This method gets called if the player is out of bounds
  public void oob() {
    level.levelReset = true;
  }
  
  //Resets the player
  public void reset() {
    vx = vy = 0;
    x = y = 300;
    jump = false;
    xDirection = 0;
    onWall = 0;
    grounded = false;
  }
  
  public void render() {
    //Rendering the player
    fill(255, 0, 255);
    
    //If pleyer dies, then play anoyher animation
    if (level.levelReset) {
      //Push the matrix, so it can me deleted
      pushMatrix();
      
      //If reset animation has run for 20 out of 30 frames, then move player
      if (level.resetAnimation >= 20) {
        //Creates a vector from player to player's starting position and moves the player with 15% of that vector
        x += (level.playerX-x)*0.15f;
        y += (level.playerY-y)*0.15f;
      }
      
      //Offsets the position by where the player needs to be rendered plus half of height
      translate((int) x+16, height-(int) y-size+16);
      //Rotates player potentially
      rotate(0.002f*pow(level.resetAnimation,2.3f));
      //Draws player, but with an offset og 16
      rect(-16, -16, 32, 32);
      //Deletes everything
      popMatrix();
    } else
      rect((int) x, height-(int) y-size, 32, 32);
  }
}
public class SolidBlock extends Block {
  public SolidBlock(int x, int y) {
    super(x, y);
    //Adds collition to the block
    solid = true;
  }
  
  public void render() {
    fill(255);
    
    rect(x*32, (int) height-y*32-32, 32, 32);
  }
}



public static class Sound {
  public static Sound TITLE_EFFECT_EXPLOSION = loadSound("Title Effect Explosion 1.wav", 60);
  public static Sound TITLE_EFFECT_BURN = loadSound("Title Effect Burning 4.wav", 60);
  public static Sound TITLE_EFFECT_BURN_LONG = loadSound("Title Effect Burning 3.wav", 60);
  public static Sound TITLE_LEATTER_APPEARING = loadSound("Title Letter Appearing 4.wav", 70);
  public static Sound TITLE_AND_KNUCKLES = loadSound("& Knuckles.wav", 70);
  public static Sound JUMP_SOUND = loadSound("Jump 1.wav", 65);
  public static Sound SELECT = loadSound("Select 1.wav", 60);
  public static Sound GOAL = loadSound("Goal 1.wav", 60);
  public static Sound ENEMY_HIT = loadSound("Enemy Hit 4.wav", 80);
  public static Sound WALL_JUMP = loadSound("Wall Jump 1.wav", 65);
  public static Sound LANDING = loadSound("Landing 1.wav", 60);
  public static Sound CRUMBLING_BLOCK = loadSound("Crumbling Block 2.wav", 60);
  
  public static Sound loadSound(String fileName, int volume) {
    Sound sound = new Sound();
    sound.volume = volume;
    try {
      AudioInputStream ais = AudioSystem.getAudioInputStream(new BufferedInputStream(please_no_steal.createInput("Sounds/"+fileName)));
      Clip clip = AudioSystem.getClip();
      clip.open(ais);
      sound.clip = clip;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    
    return sound;
  }
  
  public static Sound loadSound(String fileName) {
    return loadSound(fileName, 100);
  }

  private Clip clip;
  private float volume;
  private int lastPlayFrame;
  
  public void play() {
    if (lastPlayFrame == please_no_steal.frameCount) return;
    lastPlayFrame = please_no_steal.frameCount;
    
    try {
      new Thread() {
        public void run() {
          synchronized (clip) {
            clip.stop();
            clip.setFramePosition(0);
            
            FloatControl volumeControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            volumeControl.setValue((volumeControl.getMaximum() - volumeControl.getMinimum()) / 100.0f * volume + volumeControl.getMinimum());
            
            clip.start();
          }
        }
      }.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public void loop() {
    try {
      new Thread() {
        public void run() {
          synchronized (clip) {
            clip.stop();
            clip.setFramePosition(0);
            
            FloatControl volumeControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            volumeControl.setValue((volumeControl.getMaximum() - volumeControl.getMinimum()) / 100.0f * volume + volumeControl.getMinimum());
            
            clip.loop(Clip.LOOP_CONTINUOUSLY);
          }
        }
      }.start();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void stop() {
    clip.stop();
  }
  
  public void volume(float volume) {
    this.volume = volume;
  }
}

//Old code
/*
import processing.sound.*;

public static class Sound {
  //List of all sounds in the game

  public static Sound TITLE_EFFECT_EXPLOSION = new Sound("Title Effect Explosion 1.wav");
  public static Sound TITLE_EFFECT_BURN = new Sound("Title Effect Burning 4.wav");
  public static Sound TITLE_EFFECT_BURN_LONG = new Sound("Title Effect Burning 3.wav", 20);
  public static Sound TITLE_LEATTER_APPEARING = new Sound("Title Letter Appearing 4.wav", 20);
  public static Sound JUMP_SOUND = new Sound("Jump 1.wav", 40);
  public static Sound SELECT = new Sound("Select 1.wav", 80);
  public static Sound GOAL = new Sound("Goal 1.wav", 60);
  public static Sound ENEMY_HIT = new Sound("Enemy Hit 3", 20);

  //Variable to store information about the sounds
  private SoundFile sound;
  private String path;
  private boolean loops;
  
  Sound(String file) {
    //If the sound doesn't have a ".wav" at the end, then add it
    if (!file.endsWith(".wav")) file += ".wav";
    
    path = "Sounds/"+file;
    
    sound = new SoundFile(please_no_steal, path);
  }
  
  Sound(String file, int volumen) {
    this(file);
    volumen(volumen);
  }
  
  //Plays the sound on a different thread, to avoid lag from loading
  public void play() {
    new Thread(new Runnable() {
      public void run() {
        sound.stop();
        sound.play();
      }
    }).start();
  }
  
  public void loop() {
    if (loops) return;
    
    loops = true;
    
    new Thread(new Runnable() {
      public void run() {
        sound.loop();
      }
    }).start();
  }
  
  public void stop() {
    loops = false;
    
    sound.stop();
  }
  
  public void volumen(float volumen) {
    sound.amp(volumen / 100.0);
  }
}*/
public class TextLoader {
  private String[] text;
  
  public TextLoader(String path) {
    text = loadStrings(path);
  }
  
  public String getString(int id) {
    if (id-1 >= text.length) return text[text.length-1];
    return text[id-1];
  }
}
public class Title {
  boolean inIntro = true;
  int selected;
  
  public void render() {
    //If in intro of the title, then run special loop, else execute the normal loop
    if (inIntro) {
      //Creates a string, with the destination to the title intro
      String str = "Title Intro/IntroTitle";
      //Adds zeroes based on the number, log((int) titleScreenFrameCount)/log(10) returns how many zeroes there already is,
      //subtracting two from it, returns how many zeroes is needed for it to have three digits
      for (int i = 0; i < ((int) titleScreenFrameCount==0?2:(2-log((int) titleScreenFrameCount)/log(10))); i++) {
        str += 0;
      }
      //Adds the number and ".png" to get the destination of the frame needed
      str += (int) titleScreenFrameCount+".png";
      //Renders current frame
      image(loadImage(str), 0, 0);
    } else {
      //Creates a string, with the destination to the title intro
      String str = "Title/Title"+(((int) titleScreenFrameCount)-104)%8+".png";
      //Renders current frame
      image(loadImage(str), 0, 0);
    }
    
    //Different speeds for differant frames
    //The first 10 frames is the First row of the logo
    if (titleScreenFrameCount < 10)
      titleScreenFrameCount += 1;
    
    //We want to pause after the first frames, so we can see the title
    else if ((int) titleScreenFrameCount == 10)
      titleScreenFrameCount += 0.04f;
    
    //We want to pause the animation a little befire the fire on the letters begins burning
    else if ((int) titleScreenFrameCount == 36)
      titleScreenFrameCount += 0.05f;
    
    //We want the animation to go slower after the big burn
    else if (titleScreenFrameCount < 38)
      titleScreenFrameCount += 0.4f;
    else
      titleScreenFrameCount += 0.25f;
      
    //Add sound effect
    //Play explosion sound when "SUPER ULTIMATE JUMP BOX! 64:" hits screen
    if (titleScreenFrameCount == 10)
      Sound.TITLE_EFFECT_EXPLOSION.play();
    
    //Play loop of burning sound effect, when "TOURNAMENT EDITION" appears
    if ((int) titleScreenFrameCount == 11)
      Sound.TITLE_EFFECT_BURN_LONG.loop();
    
    //Stops loop when it isn't burning anymore
    if ((int) titleScreenFrameCount == 31)
      Sound.TITLE_EFFECT_BURN_LONG.stop();
      
    //Loops quiter version of burning sound effect when the small fire on the letters appear
    if ((int) titleScreenFrameCount == 37)
      Sound.TITLE_EFFECT_BURN.loop();
    
    //Fade burning sound effect out, when "TOURNAMENT EDITION" is finished appearing
    if ((int) titleScreenFrameCount == 45)
      Sound.TITLE_EFFECT_BURN.volume(17);
    if ((int) titleScreenFrameCount == 46)
      Sound.TITLE_EFFECT_BURN.volume(14);
    if ((int) titleScreenFrameCount == 47)
      Sound.TITLE_EFFECT_BURN.volume(11);
    if ((int) titleScreenFrameCount == 48)
      Sound.TITLE_EFFECT_BURN.volume(8);
    if ((int) titleScreenFrameCount == 49)
      Sound.TITLE_EFFECT_BURN.volume(5);
      
    //Plays & Knuckles title theme sound
    if ((int) titleScreenFrameCount == 85)
      Sound.TITLE_AND_KNUCKLES.play();
    
    //Turns down the burning even more, when the title is done appearing
    if ((int) titleScreenFrameCount == 104)
      Sound.TITLE_EFFECT_BURN.volume(2);
    
    //Plays a shooting like sound, everytime a letter appears 
    if ((((int) titleScreenFrameCount)) > 50 && (((int) titleScreenFrameCount)) < 81 && (((int) titleScreenFrameCount)-50)%2 == 0)
      Sound.TITLE_LEATTER_APPEARING.play();
    
    //If there isn't anymore intro, then loop back to the beginning
    if (titleScreenFrameCount > 104) inIntro = false;
    
    if (!inIntro) {
      //Renders options when the intro isn't playing

      fill(0);
      textSize(45);
      text("Play", width/2-textWidth("Play")/2, 340);
      text("Exit", width/2-textWidth("Exit")/2, 340+60);
      
      //Render selecter based on what is selected
      text("->", width/2-textWidth("->")-10-textWidth("Play")/2, 340+60*(selected%2));
    }
  }
  
  public void select() {
    //If in intro and player presses space, then skip intro scene
    if (inIntro) {
      titleScreenFrameCount = 104;
      inIntro = false;
      Sound.TITLE_EFFECT_BURN.stop();
      Sound.TITLE_EFFECT_BURN_LONG.stop();
      Sound.TITLE_EFFECT_EXPLOSION.stop();
      Sound.TITLE_LEATTER_APPEARING.stop();
      Sound.TITLE_EFFECT_BURN.volume(2);
      Sound.TITLE_EFFECT_BURN.loop();
      return;
    }
    
    //If selected is play, then set onTitle to false
    if (selected%2 == 0) {
      onTitle = false;
      Sound.SELECT.play();
      Sound.TITLE_EFFECT_BURN.stop();
      Sound.TITLE_EFFECT_BURN_LONG.stop();
      Sound.TITLE_EFFECT_EXPLOSION.stop();
      Sound.TITLE_LEATTER_APPEARING.stop();
    }
    //If selected is exit, then exit game
    else
      System.exit(0);
  }
}
public class WalkingEnemy extends Enemy {
  private int direction;
  
  public WalkingEnemy(float x, float y, int dir) {
    super(x, y);
    
    direction = dir;
  }
  
  public void update() {
    //If the enemy hist a wall, then turn around
    if (onWall != 0) direction = onWall;
    
    //Moves the enemy in the x dirrection based on direction
    vx = direction;
    
    //Adds gravity to enemy
    vy += level.gravity;
    
    //Moves the enemy
    move();
  }
  
  public void render() {
    //Rendering the player
    fill(0, 0, 255);
    
    rect((int) x, height-(int) y-size, 32, 32);
  }
}
  public void settings() {  size(640, 480); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "ULTIMATE_JUMP_BOX" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
